<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Pendaftaran</title>
</head>

<body>
    <div class="row">

        <div class="col" style="padding:40px; max-width:400px">

            <h1>Pendaftaran</h1>

            @if ($errors->any())

                <div class="alert alert-danger" role="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="/daftar" method="POST">
                @csrf
                <div class="mb-3">
                    <label for="Nm_pendaftar" class="form-label">Nama Pendaftar</label>
                    <input type="text" class="form-control" id="Nm_pendaftar" name="Nm_pendaftar" required>
                </div>

                <div class="mb-3">
                    <label for="Alamat" class="form-label">Alamat</label>
                    <input type="text" class="form-control" id="Alamat" name="Alamat" required>
                </div>

                <div class="mb-3">
                    <label for="Jenis_kelamin" class="form-label">Jenis Kelamin</label>
                    <br>

                    <input type="radio" id="Jenis_kelamin_wanita" name="Jenis_kelamin" value="Wanita">
                    <label for="Jenis_kelamin_wanita">wanita</label><br>
                    <input type="radio" id="Jenis_kelamin_pria" name="Jenis_kelamin" value="Pria">
                    <label for="Jenis_kelamin_pria">pria</label><br>
                </div>

                <div class="mb-3">
                    <label for="No_hp" class="form-label">Nomor HP</label>
                    <div>+62</div><input type="text" class="form-control" id="No_hp" name="No_hp" required>
                </div>

                <div class="mb-3">
                    <label for="Asal_sekolah" class="form-label">Asal Sekolah</label>
                    <input type="text" class="form-control" id="Asal_sekolah" name="Asal_sekolah" required>
                </div>

                <div class="mb-3">
                    <label for="Jurusan" class="form-label">Jurusan</label>
                    <select class="form-select" id="Jurusan" name="Jurusan" required>
                        <option value="RPL">RPL</option>
                        <option value="MM">MM</option>
                    </select>
                </div>

                <div class="mb-3">
                    <label for="Tgl_lahir" class="form-label">Tanggal Lahir</label>
                    <input type="date" class="form-control" id="Tgl_lahir" name="Tgl_lahir" required>
                </div>

                <div class="mb-3">
                    <label for="NISN" class="form-label">NISN</label>
                    <input type="text" class="form-control" id="NISN" name="NISN" required>
                </div>

                <button type="submit" class="btn btn-primary">Daftar</button>
            </form>
        </div>
        <div class="col">

            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">NO</th>
                        <th scope="col">Nama</th>

                        <th scope="col">Alamat</th>
                        <th scope="col">Jenis kelamin</th>
                        <th scope="col">No hp</th>
                        <th scope="col">Asal sekolah</th>
                        <th scope="col">Jurusan</th>
                        <th scope="col">Tgl lahir</th>
                        <th scope="col">NISN</th>

                        <th scope="col" colspan="2" style="text-align: center;">Ubah</th>


                    </tr>
                </thead>
                <tbody>
                    <?php $a = 1; ?>
                    @foreach ($data as $dat)
                        <tr>
                            <th scope="row">{{ $a++ }}</th>
                            <td>{{ $dat->Nm_pendaftar }}</td>
                            <td>{{ $dat->Alamat }}</td>
                            <td>{{ $dat->Jenis_kelamin }}</td>
                            <td>{{ $dat->No_hp }}</td>
                            <td>{{ $dat->Asal_sekolah }}</td>
                            <td>{{ $dat->Jurusan }}</td>
                            <td>{{ $dat->Tgl_lahir }}</td>
                            <td>{{ $dat->NISN }}</td>
                            <td>
                                <form action="/delete/{{ $dat->id_pendaftar }}" method="post">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-danger" onclick="confirm()">hapus</button>

                                </form>




                            </td>
                            <td> <a href="/edit/{{ $dat->id_pendaftar }}" class="btn btn-warning">Edit</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
        integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous">
    </script>
    -->
</body>

</html>
