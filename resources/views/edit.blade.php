<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Pendaftaran</title>
</head>

<body>
    <div class="row">

        <div class="col" style="padding:40px; max-width:400px">

            <h1>Pendaftaran</h1>

            @if ($errors->any())

                <div class="alert alert-danger" role="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="/edit/{{ $data->id_pendaftar }}" method="POST">
                @csrf
                @method('put')
                <div class="mb-3">
                    <label for="Nm_pendaftar" class="form-label">Nama Pendaftar</label>
                    <input type="text" value="{{ $data->Nm_pendaftar }}" class="form-control" id="Nm_pendaftar"
                        name="Nm_pendaftar" required>
                </div>

                <div class="mb-3">
                    <label for="Alamat" class="form-label">Alamat</label>
                    <input type="text" value="{{ $data->Alamat }}" class="form-control" id="Alamat" name="Alamat"
                        required>
                </div>

                <div class="mb-3">
                    <label for="Jenis_kelamin" class="form-label">Jenis Kelamin</label>
                    <br>



                    @if ($data->Jenis_kelamin === 'Wanita')
                        <input type="radio" id="Jenis_kelamin_wanita" name="Jenis_kelamin" checked value="Wanita">
                        <label for="Jenis_kelamin_wanita">wanita</label><br>
                        <input type="radio" id="Jenis_kelamin_pria" name="Jenis_kelamin" value="Pria">
                        <label for="<Jenis_kelamin_pria">pria</label><br>
                    @else
                        <input type="radio" id="Jenis_kelamin_wanita" name="Jenis_kelamin" value="Wanita">
                        <label for="Jenis_kelamin_wanita">wanita</label><br>
                        <input type="radio" id="Jenis_kelamin_pria" name="Jenis_kelamin" checked value="Pria">
                        <label for="<Jenis_kelamin_pria">pria</label><br>
                    @endif

                </div>

                <div class="mb-3">
                    <label for="No_hp" class="form-label">Nomor HP</label>
                    <div>+62</div><input type="text" value="{{ $data->No_hp }}" class="form-control" id="No_hp"
                        name="No_hp" required>
                </div>

                <div class="mb-3">
                    <label for="Asal_sekolah" class="form-label">Asal Sekolah</label>
                    <input type="text" class="form-control" value="{{ $data->Asal_sekolah }}" id="Asal_sekolah"
                        name="Asal_sekolah" required>
                </div>

                <div class="mb-3">
                    <label for="Jurusan" class="form-label">Jurusan</label>
                    <select class="form-select" id="Jurusan" name="Jurusan" required>
                        @if ($data->Jurusan === 'RPL')
                            <option value="RPL" selected="selected">RPL</option>
                            <option value="MM">MM</option>
                        @else
                            <option value="RPL">RPL</option>
                            <option value="MM" selected="selected">MM</option>
                        @endif

                    </select>
                </div>

                <div class="mb-3">
                    <label for="Tgl_lahir" class="form-label">Tanggal Lahir</label>
                    <input type="date" class="form-control" value="{{ $data->Tgl_lahir }}"id="Tgl_lahir"
                        name="Tgl_lahir" required>
                </div>

                <div class="mb-3">
                    <label for="NISN" class="form-label">NISN</label>
                    <input type="text" class="form-control" id="NISN" value="{{ $data->NISN }}" name="NISN"
                        required>
                </div>

                <button type="submit" class="btn btn-primary">Ubah</button>
            </form>
        </div>

    </div>
    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
        integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous">
    </script>
    -->
</body>

</html>
