<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\pendaftaran;



// Route::get('/', function () {
//     return view('tampilan');
// });
Route::post('/daftar', [pendaftaran::class, 'store']);
Route::get('/',[pendaftaran::class,'buka']);
Route::delete('/delete/{id}',[pendaftaran::class,'hapus']);
Route::get('/edit/{id}',[pendaftaran::class,'form_edit']);
Route::put('/edit/{id}',[pendaftaran::class,'edit']);