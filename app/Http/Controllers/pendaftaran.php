<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class pendaftaran extends Controller
{
    public function store(Request $request)
    {

        // dd($request);
       
        $validatedData = $request->validate([
            'Nm_pendaftar' => 'required|string|max:255',  // Nama pendaftar wajib dan maksimum 255 karakter
            'Alamat' => 'required|string|max:255',        // Alamat wajib dan maksimum 255 karakter
            'Jenis_kelamin' => 'required|in:Pria,Wanita', // Jenis kelamin harus "Pria" atau "Wanita"
            'No_hp' => 'required|numeric',          // Nomor HP wajib, sebaiknya sebagai string dan maksimal 20 karakter
            'Asal_sekolah' => 'required|string|max:255',  // Asal sekolah wajib dan maksimum 255 karakter
            'Jurusan' => 'required|string|max:255',       // Jurusan wajib dan maksimum 255 karakter
            'Tgl_lahir' => 'required|date',               // Tanggal lahir harus dalam format tanggal
            'NISN' => 'required|string|max:255',  
        ]);

    // echo"berhasil";

    DB::table('pendaftaran')->insert([
        'Nm_pendaftar' => $validatedData['Nm_pendaftar'],
        'Alamat' => $validatedData['Alamat'],
        'Jenis_kelamin' => $validatedData['Jenis_kelamin'],
        'No_hp' => $validatedData['No_hp'],
        'Asal_sekolah' => $validatedData['Asal_sekolah'],
        'Jurusan' => $validatedData['Jurusan'],
        'Tgl_lahir' => $validatedData['Tgl_lahir'],
        'NISN' => $validatedData['NISN']
       
    ]);

 return redirect("/");
    }
    public function buka(){
        $users = DB::table('pendaftaran')->get();
        return view('tampilan', ['data' => $users]);
    }
    public function hapus($id){
        $deleted = DB::table('pendaftaran')->where('id_pendaftar', $id)->delete();
        return redirect("/");
    }
    public function form_edit($id){
        $users = DB::table('pendaftaran')->where('id_pendaftar', $id)->get();
        return view('edit', ['data' => $users[0]]);
    }
    public function edit(request $request,$id){


        $validatedData = $request->validate([
            'Nm_pendaftar' => 'required|string|max:255',  // Nama pendaftar wajib dan maksimum 255 karakter
            'Alamat' => 'required|string|max:255',        // Alamat wajib dan maksimum 255 karakter
            'Jenis_kelamin' => 'required|in:Pria,Wanita', // Jenis kelamin harus "Pria" atau "Wanita"
            'No_hp' => 'required|numeric',          // Nomor HP wajib, sebaiknya sebagai string dan maksimal 20 karakter
            'Asal_sekolah' => 'required|string|max:255',  // Asal sekolah wajib dan maksimum 255 karakter
            'Jurusan' => 'required|string|max:255',       // Jurusan wajib dan maksimum 255 karakter
            'Tgl_lahir' => 'required|date',               // Tanggal lahir harus dalam format tanggal
            'NISN' => 'required|string|max:255',  
        ]);




        DB::table('pendaftaran')
        ->where('id_pendaftar', $id)
        ->update([
            'Nm_pendaftar' => $validatedData['Nm_pendaftar'],
            'Alamat' => $validatedData['Alamat'],
            'Jenis_kelamin' => $validatedData['Jenis_kelamin'],
            'No_hp' => $validatedData['No_hp'],
            'Asal_sekolah' => $validatedData['Asal_sekolah'],
            'Jurusan' => $validatedData['Jurusan'],
            'Tgl_lahir' => $validatedData['Tgl_lahir'],
            'NISN' => $validatedData['NISN']
    ]);

    return redirect("/");
    }
}
